require 'faraday'
require 'faraday_middleware'
require 'json'

module Shopwired
  class Client
    MAX_RESPONSE_ITEM_COUNT = 100 # http://resources.shopwired.co.uk/api/introduction/api-pagination

    ORDERS_PATH = '/v1/orders'
    ORDER_STATUSES_PATH = '/v1/order-statuses'
    STOCK_PATH = '/v1/stock'
    PRODUCTS_PATH = '/v1/products'
    SHIPPING_ZONES_PATH = '/v1/shipping-zones'
    SHIPPING_RATES_PATH = '/v1/shipping-rates'
    CUSTOMERS_PATH = '/v1/customers'
    CATEGORIES_PATH = '/v1/categories'
    BRANDS_PATH = '/v1/brands'
    BUSINESS_PATH = '/v1/business'
    FILTER_GROUPS_PATH = '/v1/filter-groups'
    WEBHOOKS_PATH = '/v1/webhooks'
    APP_DATA_PATH = '/v1/app/data'
    PAYMENT_METHODS_PATH = 'v1/payment-methods'
    PRICE_PATH = 'v1/products/prices'

    class APICallFailedError < StandardError
      attr_reader :response
      def initialize(response)
        super
        @response = response
      end

      def to_s
        "status: #{response.status} body: #{response.body}"
      end

      def inspect
        "status: #{response.status} body: #{response.body} headers: #{response.headers}"
      end
    end

    class NotFoundError < APICallFailedError; end
    class UnauthorizedError < APICallFailedError; end
    class ForbiddenError < APICallFailedError; end
    class RateLimitHitError < APICallFailedError; end

    class UnprocessableEntityError < APICallFailedError
      def parsed_body
        JSON.parse(@response.body)
      end

      def message
        parsed_body['message']
      rescue JSON::ParserError
        nil
      end

      def code
        parsed_body['code']
      rescue JSON::ParserError
        nil
      end
    end

    class NotAvailableInBasicAuthModeError < StandardError; end

    def initialize(key:, secret:, access_token:, options: {})
      if access_token.nil?
        @key = key
        @secret = secret
      else
        @access_token = access_token
      end

      @url = options[:staging] ? 'https://api.stagingdns.co.uk' : 'https://api.ecommerceapi.uk'

      @max_retries = options[:max_retries] || 20
      @retry_interval = options[:retry_interval] || 1
      @backoff_factor = options[:backoff_factor] || 1
      @retry_non_idempotent_methods = options[:retry_non_idempotent_methods] || true

      @logger = options[:logger]
    end

    # Product
    def create_product(payload)
      response = connection.post do |request|
        request.url(PRODUCTS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def products(options = {}, &block)
      get_entities(PRODUCTS_PATH, options, &block)
    end

    def product(id, options = {})
      response = connection.get do |request|
        request.url("#{PRODUCTS_PATH}/#{id}", options)
      end
      handle_response(response)
    end

    def update_product(id, payload)
      response = connection.put do |request|
        request.url("#{PRODUCTS_PATH}/#{id}")
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def stock(sku)
      response = connection.get do |request|
        request.url("#{STOCK_PATH}?sku=#{sku}")
      end
      handle_response(response)
    end

    def update_stock(sku, quantity)
      response = connection.post do |request|
        request.url(STOCK_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = { sku: sku, quantity: quantity }.to_json
      end
      handle_response(response)
    end

    def update_stock_multiple(payload)
      response = connection.post do |request|
        request.url(STOCK_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_product(product_id)
      response = connection.delete do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}")
      end
      handle_response(response)
    end

    def create_product_image(product_id, payload)
      response = connection.post do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}/images")
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    # Product option
    def product_options(product_id, options = {}, &block)
      get_entities("#{PRODUCTS_PATH}/#{product_id}/options", options, &block)
    end

    # Product variation
    def product_variations(product_id, options = {}, &block)
      get_entities("#{PRODUCTS_PATH}/#{product_id}/variations", options, &block)
    end

    def product_variation(product_id, variation_id)
      response = connection.get do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}/variations/#{variation_id}")
      end
      handle_response(response)
    end

    def create_product_variation(product_id, payload)
      response = connection.post do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}/variations")
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def update_product_variation(product_id, variation_id, payload)
      response = connection.put do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}/variations/#{variation_id}")
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_product_variation(product_id, variation_id)
      response = connection.delete do |request|
        request.url("#{PRODUCTS_PATH}/#{product_id}/variations/#{variation_id}")
      end
      handle_response(response)
    end

    # Customer
    def create_customer(payload)
      response = connection.post do |request|
        request.url(CUSTOMERS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def customers(options = {}, &block)
      get_entities(CUSTOMERS_PATH, options, &block)
    end

    def customer(customer_id)
      response = connection.get do |request|
        request.url("#{CUSTOMERS_PATH}/#{customer_id}")
      end
      handle_response(response)
    end

    def update_customer(customer_id, customer)
      response = connection.put do |request|
        request.url("#{CUSTOMERS_PATH}/#{customer_id}")
        request.headers['Content-Type'] = 'application/json'
        request.body = customer.to_json
      end
      handle_response(response)
    end

    def delete_customer(customer_id)
      response = connection.delete do |request|
        request.url("#{CUSTOMERS_PATH}/#{customer_id}")
      end
      handle_response(response)
    end

    # Order statuses
    def order_statuses(options = {}, &block)
      get_entities(ORDER_STATUSES_PATH, options, &block)
    end

    # Shipping
    def shipping_zones(options = {}, &block)
      get_entities(SHIPPING_ZONES_PATH, options, &block)
    end

    def shipping_rates(options = {}, &block)
      get_entities(SHIPPING_RATES_PATH, options, &block)
    end

    # Order
    def order_count
      response = connection.get do |request|
        request.url("#{ORDERS_PATH}/count")
      end
      handle_response(response)
    end

    def orders(options = {}, &block)
      get_entities(ORDERS_PATH, options, &block)
    end

    def order(id, options = {})
      response = connection.get do |request|
        request.url("#{ORDERS_PATH}/#{id}", options)
      end
      handle_response(response)
    end

    def create_order(payload)
      response = connection.post do |request|
        request.url(ORDERS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_order(id)
      response = connection.delete do |request|
        request.url("#{ORDERS_PATH}/#{id}")
      end
      handle_response(response)
    end

    def update_order_status(order_id, payload, send_email: nil)
      response = connection.post do |request|
        request.url("#{ORDERS_PATH}/#{order_id}/status")
        request.headers['Content-Type'] = 'application/json'
        payload[:sendEmail] = send_email
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def categories(options = {}, &block)
      get_entities(CATEGORIES_PATH, options, &block)
    end

    def create_category(payload)
      response = connection.post do |request|
        request.url(CATEGORIES_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_category(id)
      response = connection.delete do |request|
        request.url("#{CATEGORIES_PATH}/#{id}")
      end
      handle_response(response)
    end

    def brands(options = {}, &block)
      get_entities(BRANDS_PATH, options, &block)
    end

    def create_brand(payload)
      response = connection.post do |request|
        request.url(BRANDS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_brand(id)
      response = connection.delete do |request|
        request.url("#{BRANDS_PATH}/#{id}")
      end
      handle_response(response)
    end

    # Filter groups
    def filter_groups(options = {}, &block)
      get_entities(FILTER_GROUPS_PATH, options, &block)
    end

    def create_filter_group(payload)
      response = connection.post do |request|
        request.url(FILTER_GROUPS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def delete_filter_group(id)
      response = connection.delete do |request|
        request.url("#{FILTER_GROUPS_PATH}/#{id}")
      end
      handle_response(response)
    end

    # Webhook
    def create_webhook(payload)
      response = connection.post do |request|
        request.url(WEBHOOKS_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def webhook(id, options = {})
      response = connection.get do |request|
        request.url("#{WEBHOOKS_PATH}/#{id}", options) # Any option present in this endpoint?
      end
      handle_response(response)
    end

    def update_webhook(id, payload)
      response = connection.put do |request|
        request.url("#{WEBHOOKS_PATH}/#{id}")
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def webhooks(options = {}, &block)
      get_entities(WEBHOOKS_PATH, options, &block)
    end

    def verify_webhook(id)
      response = connection.post do |request|
        request.url("v1/webhooks/#{id}/verify")
        request.headers['Content-Type'] = 'application/json'
      end

      raise APICallFailedError.new(response) unless response.success?
    end

    def delete_webhook(id)
      response = connection.delete do |request|
        request.url("#{WEBHOOKS_PATH}/#{id}")
      end
      handle_response(response)
    end

    def app_data(options = {})
      raise NotAvailableInBasicAuthModeError if basic_auth_mode?

      response = connection.get do |request|
        request.url(APP_DATA_PATH, options)
      end
      handle_response(response)
    end

    def update_app_data(payload)
      raise NotAvailableInBasicAuthModeError if basic_auth_mode?

      response = connection.post do |request|
        request.url(APP_DATA_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    def business(options = {})
      response = connection.get do |request|
        request.url(BUSINESS_PATH, options)
      end
      handle_response(response)
    end

    def payment_methods(options = {}, &block)
      get_entities(PAYMENT_METHODS_PATH, options, &block)
    end
    
    def update_price(sku, price)
      response = connection.post do |request|
        request.url(PRICE_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = { sku: sku, price: price }.to_json
      end
      handle_response(response)
    end
    
    def update_price_multiple(payload)
      response = connection.post do |request|
        request.url(PRICE_PATH)
        request.headers['Content-Type'] = 'application/json'
        request.body = payload.to_json
      end
      handle_response(response)
    end

    private def connection
      @connection ||= new_connection
    end

    private def new_connection
      Faraday.new(url: @url) do |faraday|
        methods = Faraday::Request::Retry::IDEMPOTENT_METHODS
        methods += [:post, :patch] if @retry_non_idempotent_methods

        faraday.request :retry,
          max: @max_retries,
          interval: @retry_interval,
          backoff_factor: @backoff_factor,
          retry_statuses: [429, 503],
          methods: methods

        if @logger
          faraday.response :logger, @logger do |logger|
            logger.filter(/(Authorization: )"(.+)"/, '\1[REMOVED]')
          end
        end

        if @access_token.nil?
          faraday.basic_auth(@key, @secret)
        else
          faraday.request :oauth2, @access_token, token_type: :bearer
        end

        faraday.adapter Faraday.default_adapter
      end
    end

    private def handle_response(response)
      raise UnauthorizedError.new(response) if response.status == 401
      raise ForbiddenError.new(response) if response.status == 403
      raise NotFoundError.new(response) if response.status == 404
      raise UnprocessableEntityError.new(response) if response.status == 422
      raise RateLimitHitError.new(response) if response.status == 429
      raise APICallFailedError.new(response) unless response.success?
      JSON.parse(response.body) if response.status != 204 && response.body != '' # Empty body with 200 code returned when verifying webhooks
    end

    private def get_entities(path, options)
      all_entities = []

      options[:count] = MAX_RESPONSE_ITEM_COUNT if options[:count].nil?
      raise ArgumentError, "'count' must be greater than 0" if options[:count] <= 0

      offset = options[:offset] || 0
      while true
        response =
          connection.get do |request|
            request.url(path, options.merge(offset: offset))
          end

        fetched_entities = handle_response(response)

        yield fetched_entities if block_given? && fetched_entities.count > 0

        all_entities += fetched_entities

        break if fetched_entities.length < options[:count] ||
          [
            /\/v1\/countries/,
            /\/v1\/payment-methods/,
            /#{PRODUCTS_PATH}\/\d+\/choices/,
            /#{PRODUCTS_PATH}\/\d+\/customization-fields/,
            /#{PRODUCTS_PATH}\/\d+\/extras/,
            /#{PRODUCTS_PATH}\/\d+\/images/,
            /#{PRODUCTS_PATH}\/\d+\/options/,
            /#{PRODUCTS_PATH}\/\d+\/variations/,
            /#{PRODUCTS_PATH}\/\d+\/choices/
          ].any?{|p| p =~ path }
          # These endpoints don't support pagination: https://phabricator.stagingdns.co.uk/T3502#80903
          # Support added to some of them https://phabricator.stagingdns.co.uk/T3502#83783

        offset += fetched_entities.length
      end

      all_entities
    end

    private def basic_auth_mode?
      !@key.nil?
    end
  end
end
