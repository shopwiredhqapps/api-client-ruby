require 'faraday'
require 'shopwired_client'

module Shopwired
  class ClientProxy
    def initialize(access_token:, expires_at:, client_id:, client_secret:, refresh_token:, callback: nil, options: {})
      @admin_url = options[:staging] ? 'https://admin.stagingdns.co.uk' : 'https://admin.myshopwired.uk'

      @access_token = access_token
      @expires_at = expires_at

      @client_id = client_id
      @client_secret = client_secret
      @refresh_token = refresh_token

      @callback = callback

      @options = options
    end

    def client
      @client ||= Shopwired::Client.new(key: nil, secret: nil, access_token: @access_token, options: @options)
    end

    def method_missing(symbol, *args, &block)
      if @expires_at <= Time.now
        response =
          Faraday.post("#{@admin_url}/oauth/access-token") do |req|
            req.body =
              {
                client_id: @client_id,
                client_secret: @client_secret,
                refresh_token: @refresh_token
              }
          end

        raise Shopwired::Client::UnauthorizedError.new(response) if response.status == 401

        access_token_params = JSON.parse(response.body)
        @access_token = access_token_params['access_token']
        @expires_at = Time.now + access_token_params['expires_in'].to_i
        @refresh_token = access_token_params['refresh_token']

        @callback.call(@access_token, @expires_at, @refresh_token) unless @callback.nil?

        @client = nil
      end

      client.send(symbol, *args, &block)
    end
  end
end
