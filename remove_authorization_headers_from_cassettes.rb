#!/usr/bin/env ruby

# Remove "Authorization" headers from VCR.
# Also see the pre-commit git hook under git_hooks directory
Dir.glob('fixtures/**/*.yml').each do |file_name|
  cassette = File.read(file_name)

  cassette.gsub!(/$\s*Authorization:$\s*-.*$/, '')

  cassette.gsub!(/(?<=\bclient_id\=)\w+/, '')
  cassette.gsub!(/(?<=\bclient_secret=)[\w\%]+/, '')
  cassette.gsub!(/(?<=\brefresh_token=)\w+/, '')

  cassette.gsub!(/(?<="access_token":")[\w\.\-]+(?=")/, '')
  cassette.gsub!(/(?<="refresh_token":")[\w\.\-]+(?=")/, '')

  File.open(file_name, 'w') do |f|
    f.write(cassette)
  end
end
