Gem::Specification.new do |s|
  s.name        = 'shopwired_client'
  s.version     = '0.6.0'
  s.summary     = 'A library to access the Shopwired API (http://resources.shopwired.co.uk/api)'
  s.authors     = ['Arif Kalaycı', 'Nebojša Stričević']
  s.files       = ['lib/shopwired_client.rb', 'lib/client.rb', 'lib/client_proxy.rb']

  s.add_dependency 'faraday', '~> 0.15'
  s.add_dependency "faraday_middleware", "~> 0.13"

  s.add_development_dependency "rspec", "~> 3.8"
  s.add_development_dependency "vcr", "~> 5.0"
  s.add_development_dependency "webmock", "~> 3.6"
end
