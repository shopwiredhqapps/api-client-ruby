require 'shopwired_client'

describe Shopwired::Client, :vcr do
  context "In basic auth mode" do
    let(:client) { Shopwired::Client.new(key: '_', secret: '_', access_token: nil, options: { staging: true }) }

    include_examples "makes API calls"

    it "retrieves all entities with custom offset and count" do
      length = client.products.length

      # Setup
      created_products = []
      2.times{ created_products << client.create_product(title: 'test') }

      retrieved_products =
        client.products(offset: length, count: 1) do |batch|
          expect(batch.length).to eq(1)
        end

      expect(retrieved_products.length).to eq(2)

      # Cleanup
      created_products.each{|p| client.delete_product(p['id']) }
    end

    it "raises error when little than 0 count given" do
      expect{ client.products(count: 0) }.to raise_error(ArgumentError)
    end

    it "retries when rate limit is hit" do
      client.order_statuses
    end

    it "handles unprocessable entity errors" do
      MAX_FILTER_GROUP_NUMBER = 15

      filter_groups = []
      MAX_FILTER_GROUP_NUMBER.times do |i|
        filter_groups << client.create_filter_group(title: "Test filter group #{i}")
      end

      begin
        client.create_filter_group(title: "Test filter group #{MAX_FILTER_GROUP_NUMBER + 1}")
      rescue Shopwired::Client::UnprocessableEntityError => e
        expect(e.code).to eq(11)
        expect(e.message).to eq("A limit was reached (number of filter groups)")
      end

      filter_groups.each do |fg|
        client.delete_filter_group(fg['id'])
      end
    end

    context "Without retry mechanism" do
      let(:client) { Shopwired::Client.new(key: '_', secret: '_', access_token: nil, options: { staging: true, max_retries: 0 }) }

      it "raises RateLimitHitError when rate limit is hit" do
        expect{ client.order_statuses }.to raise_error(Shopwired::Client::RateLimitHitError)
      end
    end
  end

  context "In oauth2 mode" do
    let(:client) { Shopwired::Client.new(key: nil, secret: nil, access_token: '_', options: { staging: true }) }
    include_examples "makes API calls"

    it "retrieves and updates app data" do
      value = 'test'
      expect(client.update_app_data(name: value)['name']).to eq(value)
      expect(client.app_data['name']).to eq(value)

      # Cleanup
      client.update_app_data(name: '')
    end
  end
end
