require 'shopwired_client'

describe Shopwired::ClientProxy, :vcr do
  context "With working access token" do
    let(:client) do
      Shopwired::ClientProxy.new(
        access_token: '_',
        expires_at: Time.now + 10,
        client_id: '_',
        client_secret: '_',
        refresh_token: '_',
        options: { staging: true }
      )
    end

    it "retrieves orders" do
      expect(client.orders.length).to eq(1)
    end
  end

  context "With expired access token" do
    let(:client) do
      Shopwired::ClientProxy.new(
        access_token: '_',
        expires_at: Time.now - 10,
        client_id: '_',
        client_secret: '_',
        refresh_token: '_',
        options: { staging: true }
      )
    end

    it "retrieves orders" do
      expect(client.orders.length).to eq(1)
    end
  end
end
