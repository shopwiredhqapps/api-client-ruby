shared_examples "makes API calls" do
  it "creates, retrieves, updates and deletes products" do
    product = client.create_product(title: 'Test Product', sku: 'TEST')
    expect(product['title']).to eq('Test Product')
    expect(client.products.length).to eq(3)
    expect(client.update_product(product['id'], title: 'Test Product Updated')['title']).to eq('Test Product Updated')
    expect(client.product(product['id'])['id']).to eq(product['id'])
    expect(client.update_stock('TEST', 1)['quantity']).to eq(1)
    expect(client.delete_product(product['id'])).to be_nil
  end

  it "updates multiple stock" do
    product1 = client.create_product(title: 'Test Product 1', sku: 'TEST1')
    product2 = client.create_product(title: 'Test Product 2', sku: 'TEST2')
    result = client.update_stock_multiple({items: [{ sku: "TEST1", quantity: 5 }, { sku: "TEST2", quantity: 10 }]})
    expect(result['updated']).to eq(2)

    # Cleanup
    expect(client.delete_product(product1['id'])).to be_nil
    expect(client.delete_product(product2['id'])).to be_nil
  end

  it "creates product image" do
    product = client.create_product(title: 'Test Product')

    description = 'Test description'
    product_image = client.create_product_image(product['id'],
      image: 'https://cdn.ecommercedns.uk/files/6/212076/4/1307304/unknown.png',
      description: description,
      sortOrder: 1
    )

    # Cleanup
    client.delete_product(product['id'])

    expect(product_image['description']).to eq(description)
  end

  it "creates, retrieves, updates and deletes product variations" do
    # Build fixture
    product = client.products.first
    product_options = client.product_options(product['id'], embed: 'product_option_values')

    variation = client.create_product_variation(product['id'], values: [product_options[0]['values'][0]['id']])
    expect(variation['values'][0]['name']).to eq('Blue')

    expect(client.product_variations(product['id']).length).to eq(2)

    stock = 999999
    expect(client.update_product_variation(product['id'], variation['id'], stock: stock)['stock']).to eq(stock)

    expect(client.product_variation(product['id'], variation['id'])['values'][0]['name']).to eq('Blue')

    expect(client.delete_product_variation(product['id'], variation['id'])).to be_nil
  end

  it "creates, retrieves, updates and deletes customers" do
    customer = client.create_customer({ "firstName": "John", "lastName": "Doe", "password": "password" })
    expect(customer['firstName']).to eq('John')
    expect(client.customers.length).to eq(1)
    expect(client.update_customer(customer['id'], firstName: 'Joe')['firstName']).to eq('Joe')
    expect(client.customer(customer['id'])['id']).to eq(customer['id'])
    expect(client.delete_customer(customer['id'])).to be_nil
  end

  it "lists order statuses" do
    expect(client.order_statuses.length).to eq(9)
  end

  it "lists shipping zones and rates" do
    expect(client.shipping_zones.length).to eq(1)
    expect(client.shipping_rates.length).to eq(3)
  end

  it "creates, retrieves and deletes orders" do
    order = client.create_order(
      {
        "billingAddress"=>
          {
            "name" => "John Doe",
            "companyName" => "",
            "emailAddress" => "johndoe@example.com",
            "telephone" => "1111111111",
            "addressLine1" => "1236 Woodcrest Lane",
            "addressLine2" => "The Mailbox, Wharfside Street",
            "addressLine3" => "",
            "city" => "East Lansing",
            "postcode" => "B1 1AY",
            "country" => "222"
          },
        "shippingAddress" =>
          {
            "name" => "John Doe",
            "companyName" => "",
            "emailAddress" => "johndoe@example.com",
            "telephone" => "1111111111",
            "addressLine1" => "1236 Woodcrest Lane",
            "addressLine2" => "The Mailbox, Wharfside Street",
            "addressLine3" => "",
            "city" => "East Lansing",
            "postcode" => "B1 1AY",
            "country" => "222"
          },
        "products" => [{"id" => "75554", "quantity" => "1"}],
        "shippingRate" => "447",
        "status" => "491"
      }
    )
    expect(order['billingAddress']['name']).to eq('John Doe')
    expect(client.orders.length).to eq(1)
    expect(client.order_count['count']).to eq(1)
    expect(client.order(order['id'])['id']).to eq(order['id'])

    # Delete automatically created customer to avoid pollution
    # WARNING: This customer (John Doe) will will be deleted even if it was already present before the test
    client.delete_customer(order['customer']['id'])

    expect(client.delete_order(order['id'])).to be_nil
  end

  it "updates orders" do
    order = client.create_order(
      {
        "billingAddress"=>
          {
            "name" => "John Doe",
            "companyName" => "",
            "emailAddress" => "johndoe@example.com",
            "telephone" => "1111111111",
            "addressLine1" => "1236 Woodcrest Lane",
            "addressLine2" => "The Mailbox, Wharfside Street",
            "addressLine3" => "",
            "city" => "East Lansing",
            "postcode" => "B1 1AY",
            "country" => "222"
          },
        "shippingAddress" =>
          {
            "name" => "John Doe",
            "companyName" => "",
            "emailAddress" => "johndoe@example.com",
            "telephone" => "1111111111",
            "addressLine1" => "1236 Woodcrest Lane",
            "addressLine2" => "The Mailbox, Wharfside Street",
            "addressLine3" => "",
            "city" => "East Lansing",
            "postcode" => "B1 1AY",
            "country" => "222"
          },
        "products" => [{"id" => "75554", "quantity" => "1"}],
        "shippingRate" => "447",
        "status" => "491"
      }
    )

    client.update_order_status(order['id'], { status: 493, trackingUrl: 'http://www.example.com/track/1' })
    updated_order = client.order(order['id'])
    expect(updated_order['status']['id']).to eq(493)
    expect(updated_order['trackingUrl']).to eq('http://www.example.com/track/1')

    # Cleanup
    client.delete_customer(order['customer']['id'])
    client.delete_order(order['id'])
  end

  it "retrieves products with embedded variations" do
    products = client.products(embed: 'variations')
    expect(products[0]['variations'].length).to eq(1)
  end

  it "creates, retrieves and deletes categories" do
    title = 'Test Category'
    category = client.create_category(title: title)
    expect(category['title']).to eq(title)
    expect(client.categories.length).to eq(3)
    expect(client.delete_category(category['id'])).to be_nil
  end

  it "creates, retrieves and deletes brands" do
    title = 'Test Brand'
    brand = client.create_brand(title: title)
    expect(brand['title']).to eq(title)
    expect(client.brands.length).to eq(1)
    expect(client.delete_brand(brand['id'])).to be_nil
  end

  it "creates and retrieves filter groups" do
    title = 'Test Filter Group'
    filter_group = client.create_filter_group(title: title, sortOrder: 1)
    expect(filter_group['title']).to eq(title)
    expect(client.filter_groups.length).to eq(1)

    expect(client.delete_filter_group(filter_group['id'])).to be_nil
  end

  it "creates, retrieves, updates, verifies and deletes webhooks" do
    topic = 'product.created'
    webhook = client.create_webhook(topic: topic, url: 'https://www.example.com/product_created')
    expect(webhook['topic']).to eq(topic)
    expect(client.webhooks.length).to eq(1)

    expect(client.webhook(webhook['id'])['id']).to eq(webhook['id'])

    new_url = 'https://www2.example.com/product_created'
    expect(client.update_webhook(webhook['id'], url: new_url)['url']).to eq(new_url)

    expect(client.verify_webhook(webhook['id'])).to be_nil
    expect(client.delete_webhook(webhook['id'])).to be_nil
  end

  it "retrieves business" do
    business = client.business(embed: 'apps,settings,extensions')
  end

  it "retrieves payment methods" do
    expect(client.payment_methods.length).to eq(2)
  end
end
